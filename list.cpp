#include <iostream>
#include "list.h"
using namespace std;

template <class T>
List<T>::~List() {
	for (Node<T> *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
template <class T>
void List<T>::pushToHead(T el)
{
	head = new Node<T>(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
template <class T>
void List<T>::pushToTail(T el)
{
	Node<T> *data = new Node<T>(el, 0);//create new node and input new value
	if (tail == NULL) {//if there no character
		head = data;//head pont new value
		tail = head;//tail point new value
	}
	else {
		tail->next = data;//point the new value
		data->prev = tail;
		tail = data;//point tail to new value
	}
}
template <class T>
T List<T>::popHead()
{
	T el = head->data;
	Node<T> *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}
template <class T>
T List<T>::popTail()
{
	T el = tail->data;
	Node<T> *tmp = tail;//create node tmp
	if (head == NULL && tail == NULL)//if no character in list return 0
	{
		return 0;
	}
	else {
		tail = tail->prev;//point tail to previous value
		tail->next = NULL;//point NULL
	}
	delete tmp;//delete the last value
	return el;
}
template <class T>
bool List<T>::search(T el)
{
	Node<T> *tmp = new Node<T>(*head);//create variable tmp
	while (tmp != 0) 
	{//run until NULL
		if (tmp->data == el) 
		{
			return true;//when found the character return true
		}
		tmp = tmp->next;//point next tmp
	}return false;
	//when didn't found character return false
	//
	//delete tmp;
	//
	//delete remain value
}
template <class T>
void List<T>::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node<T> *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}