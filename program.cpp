#include <iostream>
#include "list.h"
#include "list.cpp"

using namespace std;
void main()
{
	//Sample Code
	
	List<char> mylist;
	
	mylist.pushToTail('K');
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.pushToTail('C');
	mylist.pushToTail('o');
	mylist.pushToTail('s');
	mylist.pushToTail('h');
	mylist.pushToTail('A');
	mylist.popTail();//call function poptail
	mylist.print();
	cout << endl;
	char input;//create char value for search
	cout << "Input a character for Search : ";
	cin >> input;//recieve character
	if (mylist.search(input) == true) 
	{//call search function if it true show cout
		cout << "Character found\n";
	}
	else 
	{//call search function if it false show below cout
		cout << "Character didn't found!\n";
	}
	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
	system("pause");
}